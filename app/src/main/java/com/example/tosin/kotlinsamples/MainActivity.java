package com.example.tosin.kotlinsamples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private String bankCode = "011";
    private String accountNumber = "0000014579";
    private int[] algo = new int[] {3, 7, 3, 3, 7, 3, 3, 7, 3, 3, 7, 3};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        System.out.println("Hellothere " + validateNubanAccount(bankCode, accountNumber));

    }

    boolean validateNubanAccount(String bankCode, String accountNumber){

        if (bankCode.length() != 3 || accountNumber.length() != 10) {
            return false;
        }

        String nubanSerialNo = accountNumber.substring(0, accountNumber.length()-1);
        String nubanAccNo = bankCode + nubanSerialNo;

        int checkDigit = Character.getNumericValue(accountNumber.charAt(accountNumber.length()-1));

        Log.d("MainActivity", String.valueOf(checkDigit));

        int sum = 0;
        for(int j=0;  j<nubanAccNo.length(); j++ ){
            sum += Character.getNumericValue(nubanAccNo.charAt(j)) * algo[j];
        }

        int validatedCheckDigit = 10 - (sum % 10);

        return checkDigit == (validatedCheckDigit % 10);

        //return false;
    }
}
