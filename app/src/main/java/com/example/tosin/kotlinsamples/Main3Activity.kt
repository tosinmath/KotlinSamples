package com.example.tosin.kotlinsamples

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

class Main3Activity : AppCompatActivity() {

    private val bankCode = "011"
    private val accountNumber = "0000014579"
    private val algo = intArrayOf(3, 7, 3, 3, 7, 3, 3, 7, 3, 3, 7, 3)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        println("Hellothere " + validateNubanAccount(bankCode, accountNumber))

    }

    internal fun validateNubanAccount(bankCode: String, accountNumber: String): Boolean {

        if (bankCode.length != 3 || accountNumber.length != 10) {
            return false
        }

        val nubanSerialNo = accountNumber.substring(0, accountNumber.length - 1)
        val nubanAccNo = bankCode + nubanSerialNo

        val checkDigit = Character.getNumericValue(accountNumber[accountNumber.length - 1])

        Log.d("MainActivity", checkDigit.toString())

        var sum = 0
        for (j in 0..nubanAccNo.length - 1) {
            sum += Character.getNumericValue(nubanAccNo[j]) * algo[j]
        }

        val validatedCheckDigit = 10 - sum % 10

        return checkDigit == validatedCheckDigit

        //return false;
    }
}
