package com.example.tosin.kotlinsamples

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MainActivity2 : AppCompatActivity() {

    val a: Int = 1;
    val b = 2;
    private val logger = Logger.getLogger(javaClass);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        logger.debug( sum(1, 5).toString() );


        val aa = 0*3+1*7+1*3+0*3+0*7+0*3+0*3+0*7+1*3+4*3+5*7+7*3;

        val bb = 0*3+1*7+1*3+0*3+0*7+0*3+0*3+0*7+0*3+0*3+2*7+2*3;

        logger.debug( aa.toString() );
        logger.debug( bb.toString() );



    }



    fun sum(a: Int, b: Int): Int {
        return a + b;
    }
}
